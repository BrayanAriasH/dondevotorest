# -*- coding: utf-8 -*-
import webapp2
from webapp2_extras import json
import urllib2, os, re, time
 
class DondeVotoMain(webapp2.RequestHandler):
    def get(self):        
        self.response.content_type = 'application/json'
        
        obj = {
                'cedula': '1503',
                'informacion': 'El siguiente es su puesto de votación',
                'dptovotacion': 'META', 
                'munvotacion': 'VILLAVICENCIO',
                'puestovotacion': 'PUESTO DE VOTACIÓN',
                'dirvotacion': 'DIRECCIÓN VOTACIÓN',
                'fecharegistro': '15 de Marzo de 2010',
                'mesavotacion': '30',
                'urlads': 'default',
                'error' : 'N',
            }
        self.response.write(json.encode(obj))

class DondeVoto(webapp2.RequestHandler):
    def get(self, userid):        
        self.response.content_type = 'application/json'
        if userid == u'1503':
            obj = {
                'cedula': userid,
                'informacion': 'El siguiente es su puesto de votación',
                'dptovotacion': 'META', 
                'munvotacion': 'VILLAVICENCIO',
                'puestovotacion': 'PUESTO DE VOTACIÓN',
                'dirvotacion': 'DIRECCIÓN VOTACIÓN',
                'fecharegistro': '15 de Marzo de 2010',
                'mesavotacion': '30',
                'urlads': 'default',
                'error': 'N',
            }
        else:
            try:
                req = urllib2.Request('http://www3.registraduria.gov.co/censo/_censoresultado.php?nCedula=%s' %(userid))
                webresult = urllib2.urlopen(req)
                text = webresult.read().decode('iso-8859-1')
                #text = text.encode('utf-8')
                webresult.close()
                datos = []
                data = text[text.find('<table width="580" align="center" cellpadding="0" cellspacing="1">'): text.find("</table>")]
                data = data.encode('utf8')
                list = re.findall(r'<[^>]*>', data)
                for key in list:
                  data = data.replace(key,'')
                split = data.split('\n')
                datos = []
                for pos in range(len(split)):
                  if pos % 2 != 0 and split[pos].strip() !='':
                    datos.append(split[pos].strip())
                if len(datos) >= 6:
                    obj = {
                        'cedula': userid,
                        'informacion': 'Resultado de la informacion del lugar',
                        'dptovotacion': datos[0], 
                        'munvotacion': datos[1],
                        'puestovotacion': datos[2],
                        'dirvotacion': datos[3],
                        'fecharegistro': datos[4],
                        'mesavotacion': datos[5],
                        'urlads': 'default',
                        'error': 'N',
                    }
                else: 
                    info = text[text.find('<div id="info">'): text.find('<div id="div_aviso">')]
                    #info = text.find('<div id="info">')
                    datainfo = re.findall(r'<[^>]*>', info)
                    for key in datainfo:
                      info = info.replace(key,'')
                    split = info.split('\n')
                    datos = []
                    for pos in range(len(split)):
                      if split[pos].strip() !='':
                        datos.append(split[pos].strip())
                    obj = {
                        'cedula': userid,
                        'informacion': datos[0],
                        'dptovotacion': '', 
                        'munvotacion': '',
                        'puestovotacion': '',
                        'dirvotacion': '',
                        'fecharegistro': '',
                        'mesavotacion': '',
                        'urlads': 'default',
                        'error': 'S'
                    }
            except Exception, e:
                #print 'Exception'
                #print e
                #print '*********'
                obj = {
                        'cedula': userid,
                        'informacion': 'Se presentaron problemas de comunicación con la página de la registraduría.',
                        'dptovotacion': '', 
                        'munvotacion': '',
                        'puestovotacion': '',
                        'dirvotacion': '',
                        'fecharegistro': '',
                        'mesavotacion': '',
                        'urlads': 'default',
                        'error': 'S'
                    }
        self.response.write(json.encode(obj))
 
 
# URLs
app = webapp2.WSGIApplication([
    ('/', DondeVotoMain),
    (r'/(\d+)', DondeVoto),
], debug=True)
